import React, { useState, useEffect } from "react";
import { Drawer, IconButton, List } from "@material-ui/core";
import {
  Home as HomeIcon,
  QuestionAnswer as SupportIcon,
  LibraryBooks as LibraryIcon,
  HelpOutline as FAQIcon,
  ArrowBack as ArrowBackIcon,
  LocalShipping as LocalShippingIcon,
  BarChart as BarChartIcon,
  Settings as SettingsIcon,
  People as PeopleIcon
} from "@material-ui/icons";

import { useTheme } from "@material-ui/styles";
import { withRouter } from "react-router-dom";
import classNames from "classnames";

// styles
import useStyles from "./styles";

// components
import SidebarLink from "./components/SidebarLink/SidebarLink";

// context
import {
  useLayoutState,
  useLayoutDispatch,
  toggleSidebar
} from "../../context/LayoutContext";

const structure = [
  { id: 0, label: "Panel", link: "/app/dashboard", icon: <HomeIcon /> },

  {
    id: 1,
    label: "Tranportistas",
    link: "/app/transportista",
    icon: <LocalShippingIcon />
  },
  {
    id: 2,
    label: "Clientes",
    link: "/app/typography",
    icon: <PeopleIcon />
  },
  /*{
    id: 3,
    label: "Reportes",
    link: "/app/typography",
    icon: <BarChartIcon />
  },*/
  {
    id: 4,
    label: "Opciones",
    link: "/app/ui",
    icon: <SettingsIcon />,
    children: [
      { label: "Tipos Vehiculos", link: "/app/tipo" },
      { label: "Marcas", link: "/app/mark" },
      { label: "Colores", link: "/app/color" },
      { label: "Ocupaciones", link: "/app/ocupacion" }
    ]
  },
  { id: 5, type: "divider" },
  { id: 6, type: "title", label: "Ayudas" },
  { id: 7, label: "uno", link: "", icon: <LibraryIcon /> },
  { id: 8, label: "dos", link: "", icon: <SupportIcon /> },
  { id: 9, label: "tres", link: "", icon: <FAQIcon /> }
];

function Sidebar({ location }) {
  var classes = useStyles();
  var theme = useTheme();

  // global
  var { isSidebarOpened } = useLayoutState();
  var layoutDispatch = useLayoutDispatch();

  // local
  var [isPermanent, setPermanent] = useState(true);

  useEffect(function() {
    window.addEventListener("resize", handleWindowWidthChange);
    handleWindowWidthChange();
    return function cleanup() {
      window.removeEventListener("resize", handleWindowWidthChange);
    };
  });

  return (
    <Drawer
      variant={isPermanent ? "permanent" : "temporary"}
      className={classNames(classes.drawer, {
        [classes.drawerOpen]: isSidebarOpened,
        [classes.drawerClose]: !isSidebarOpened
      })}
      classes={{
        paper: classNames({
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened
        })
      }}
      open={isSidebarOpened}
    >
      <div className={classes.toolbar} />
      <div className={classes.mobileBackButton}>
        <IconButton onClick={() => toggleSidebar(layoutDispatch)}>
          <ArrowBackIcon
            classes={{
              root: classNames(classes.headerIcon, classes.headerIconCollapse)
            }}
          />
        </IconButton>
      </div>
      <List className={classes.sidebarList}>
        {structure.map(link => (
          <SidebarLink
            key={link.id}
            location={location}
            isSidebarOpened={isSidebarOpened}
            {...link}
          />
        ))}
      </List>
    </Drawer>
  );

  // ##################################################################
  function handleWindowWidthChange() {
    var windowWidth = window.innerWidth;
    var breakpointWidth = theme.breakpoints.values.md;
    var isSmallScreen = windowWidth < breakpointWidth;

    if (isSmallScreen && isPermanent) {
      setPermanent(false);
    } else if (!isSmallScreen && !isPermanent) {
      setPermanent(true);
    }
  }
}

export default withRouter(Sidebar);
