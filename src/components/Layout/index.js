import React from "react";
import { Route, Switch, withRouter } from "react-router-dom";
import classnames from "classnames";

// styles
import useStyles from "./styles";

// components
import Header from "../Header/Header";
import Sidebar from "../Sidebar/Sidebar";
import Create from "../../pages/Transportista/Create";
import ShowV from "../../pages/Transportista/Show";
import CreateTipo from "../../pages/TipoVehiculo/Create";
import EditTipo from "../../pages/TipoVehiculo/Edit";
import ShowTipo from "../../pages/TipoVehiculo/Show";
import EditV from "../../pages/Transportista/Edit";
import Mark from "../../pages/Marca";
import MarkAdd from "../../pages/Marca/Create";
import MarkShow from "../../pages/Marca/Show";
import MarkEdit from "../../pages/Marca/Edit";
// pages
import Dashboard from "../../pages/dashboard/Dashboard";
import Trasportista from "../../pages/Transportista";
import TipoVehiculo from "../../pages/TipoVehiculo";
//Pagina Colores
import Color from "../../pages/Color";
import ColorAdd from "../../pages/Color/Create";
import ColorShow from "../../pages/Color/Show";
import ColorEdit from "../../pages/Color/Edit";
//Pagina Ocupaciones
import Ocupacion from "../../pages/Ocupacion";
import OcupacionAdd from "../../pages/Ocupacion/Create";
import OcupacionShow from "../../pages/Ocupacion/Show";
import OcupacionEdit from "../../pages/Ocupacion/Edit";

// context
import { useLayoutState } from "../../context/LayoutContext";

function Layout(props) {
  var classes = useStyles();

  // global
  var layoutState = useLayoutState();

  return (
    <div className={classes.root}>
      <>
        <Header history={props.history} />
        <Sidebar />
        <div
          className={classnames(classes.content, {
            [classes.contentShift]: layoutState.isSidebarOpened
          })}
        >
          <div className={classes.fakeToolbar} />
          <Switch>
            <Route path="/app/dashboard" component={Dashboard} />
            {/* Transportista */}
            <Route exact path="/app/transportista/add" component={Create} />
            <Route exact path="/app/transportista/show/:id" component={ShowV} />
            <Route exact path="/app/transportista/edit/:id" component={EditV} />
            <Route path="/app/transportista" component={Trasportista} />
            {/* Tipo Vehiculo */}
            <Route exact path="/app/tipo/add" component={CreateTipo} />
            <Route exact path="/app/tipo/edit/:id" component={EditTipo} />
            <Route exact path="/app/tipo/show/:id" component={ShowTipo} />
            <Route path="/app/tipo" component={TipoVehiculo} />
            {/* Marcas */}
            <Route exact path="/app/mark/add" component={MarkAdd} />
            <Route exact path="/app/mark/edit/:id" component={MarkEdit} />
            <Route exact path="/app/mark/show/:id" component={MarkShow} />
            <Route path="/app/mark" component={Mark} />
            {/* Colores */}
            <Route exact path="/app/color/add" component={ColorAdd} />
            <Route exact path="/app/color/edit/:id" component={ColorEdit} />
            <Route exact path="/app/color/show/:id" component={ColorShow} />
            <Route path="/app/color" component={Color} />
            {/* Ocupaciones */}
            <Route exact path="/app/ocupacion/add" component={OcupacionAdd} />
            <Route
              exact
              path="/app/ocupacion/edit/:id"
              component={OcupacionEdit}
            />
            <Route
              exact
              path="/app/ocupacion/show/:id"
              component={OcupacionShow}
            />
            <Route path="/app/ocupacion" component={Ocupacion} />
          </Switch>
        </div>
      </>
    </div>
  );
}

export default withRouter(Layout);
