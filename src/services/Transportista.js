import axios from "axios";
const USER_API_BASE_URL = "http://localhost:8000/api/";

export const login = user => {
  return axios
    .get(
      USER_API_BASE_URL + "auth/login",
      {
        email: user.email,
        password: user.password
      },
      {
        headers: { "Content-Type": "application/json" }
      }
    )
    .then(response => {
      localStorage.setItem("access_token", response.data.access_token);
      localStorage.setItem("id", response.data.user.id);
      localStorage.setItem("people_id", response.data.user.people_id);
      console.log(response.data);
      return response.data;
    })
    .catch(err => {
      console.log(err.response);
      // return err.response;
    });
};
//Obtener todos los Recursos get para todos los tipos de consulta
export const getItem = url => {
  return new Promise((resolve, reject) => {
    axios
      .get(url)
      .then(res => {
        resolve(res.data);
      })
      .catch(err => reject(err));
  });
};
