import axios from 'axios';

const USER_API_BASE_URL = 'http://localhost:8000/api/';

export const login = user => {
    return axios
        .post(
            USER_API_BASE_URL+'auth/login',
            {
                email: user.email,
                password: user.password
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(response => {
            localStorage.setItem('access_token', response.data.access_token);
            localStorage.setItem('id', response.data.user.id);
            localStorage.setItem('people_id', response.data.user.people_id);
            console.log(response.data)
            return response.data;
            
        })
        .catch(err => {
          console.log(err.response)
       // return err.response;
        })
}
export const datosUsuario = user => {
    return axios
        .post(
            USER_API_BASE_URL+'user',
            {
                id: user.id,
                people_id: user.people_id
            },
            {
                headers: { 'Content-Type': 'application/json' }
            }
        )
        .then(response => {
            console.log(response.data)
            return response.data;
            
        })
        .catch(err => {
          console.log(err.response)
       // return err.response;
        })
}