import React, { useEffect, useState, Fragment } from "react";
import { Paper } from "@material-ui/core";
import { UPLOAD_ENDPOINT, IMAGES } from "../../config";
// styles
import useStyles from "./styles";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
// components
import PageTitle from "../../components/PageTitle/PageTitle";

// icons sets
import "font-awesome/css/font-awesome.min.css";
import axios from "axios";
function View(props) {
  const [datos, setDatos] = useState();
  const [load, setLoad] = useState(false);
  var classes = useStyles();
  useEffect(() => {
    const { id } = props.match.params;
    const editando = async () => {
      const formData = new FormData();
      formData.append("id", id);
      const tipo = {
        headers: {
          Accept: "*/*",
          "Content-Type": "application/x-www-form-urlencoded",
          "X-Requested-With": "XMLHttpRequest"
        }
      };
      try {
        const response = await axios.post(
          UPLOAD_ENDPOINT + "web/driver/show",
          formData,
          tipo
        );
        console.log(response.data);
        setDatos(response.data);
        setLoad(true);
      } catch (err) {
        console.log("dentro del upload", err.response);
      }
    };
    editando();
  }, [props.match.params]);
  if (load) {
    return (
      <Fragment>
        <Paper className={classes.iconsContainer}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <PageTitle
                title="Detalle Transportista"
                button="Regresar"
                url="/app/transportista"
              />
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="h6" variant="h6" display="inline">
                Nombre Completo :
              </Typography>
              {datos[0].full_name}
              <Typography
                component="p"
                variant="subtitle1"
                display="inline"
              ></Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Carnet de Identidad :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].ci}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Fecha Nacimiento :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].birthday}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Email :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].email}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Celular :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].mobile}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Telefono :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].telephone}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Número de Cuenta :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].number_account}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Departamento :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].department}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Dirección :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].address}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="h6" variant="h6" display="inline">
                Imagen Conductor :
              </Typography>
              <Grid item xs={12} sm={4}>
                <img
                  src={IMAGES + datos[0].url_profile}
                  alt=""
                  height="150"
                  width="150"
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="h6" variant="h6" display="inline">
                Imagen Carnet Identidad :
              </Typography>
              <Grid item xs={12} sm={4}>
                <img
                  src={IMAGES + datos[0].url_ci}
                  alt=""
                  height="150"
                  width="150"
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="h6" variant="h6" display="inline">
                Imagen Brevet :
              </Typography>
              <Grid item xs={12} sm={4}>
                <img
                  src={IMAGES + datos[0].url_brevet}
                  alt=""
                  height="150"
                  width="150"
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={12}>
              <PageTitle title="Datos Vehiculo" />
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Nombre Propietario :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].car_owner}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Número de Placa :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].license_plate}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Modelo :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].model}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Tipo de Vehículo :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].type_vehicle}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Marca :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].mark}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="p" variant="h6" display="inline">
                Color :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].color}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={4}>
              <Typography component="h6" variant="h6" display="inline">
                Imagen Vehículo :
              </Typography>
              <Grid item xs={12} sm={4}>
                <img
                  src={IMAGES + datos[0].url_car}
                  alt=""
                  height="150"
                  width="150"
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="h6" variant="h6" display="inline">
                Imagen Ruat :
              </Typography>
              <Grid item xs={12} sm={4}>
                <img
                  src={IMAGES + datos[0].url_ruat}
                  alt=""
                  height="150"
                  width="150"
                />
              </Grid>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Typography component="h6" variant="h6" display="inline">
                Imagen Soat :
              </Typography>
              <Grid item xs={12} sm={4}>
                <img
                  src={IMAGES + datos[0].url_soat}
                  alt=""
                  height="150"
                  width="150"
                />
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
export default View;
