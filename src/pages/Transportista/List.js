import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import {
  Grid,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  Button,
  IconButton,
  Snackbar
} from "@material-ui/core";
import axios from "axios";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import VisibilityIcon from "@material-ui/icons/Visibility";
import Notification from "../../components/Notification/Notification";
import { UPLOAD_ENDPOINT } from "../../config";
import { Link } from "react-router-dom";
import Slide from "@material-ui/core/Slide";
import { Edit as EditIcon, Delete as DeleteIcon } from "@material-ui/icons";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
export default function List(props) {
  const [openD, setOpenD] = React.useState(false);
  const [idU, setIdU] = React.useState("");
  const [openV, setOpenV] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  // Declare a local state to be used internally by this component
  const { datos } = props;

  const table = [
    "#",
    "Nombre Completo",
    "Carnet Identidad",
    "Celular",
    "Acciones"
  ];

  //funciones
  const handleClickOpenD = (e, id) => {
    setOpenD(true);
    setIdU(id);

    console.log(id);
  };
  const handleCloseD = () => {
    setOpenD(false);
  };
  const handleCloseV = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenV(false);
  };
  let history = useHistory();
  //enviar formulario
  const onSubmit = async e => {
    setOpenD(false);
    e.preventDefault();
    console.log(idU);
    setIsLoading(true);

    return await uploadFile(idU).then(res => {
      if (res.status === 200) {
        console.log("Eliminado Correctamente", res.status);
        setIsLoading(false);
        setOpenV(true);
      }
      history.push("/app/dashboard");
      //window.location.reload(false);
    });
  };
  //enviar datos al servidor
  const uploadFile = async idU => {
    const formData = new FormData();
    formData.append("id", idU);
    const tipo = {
      headers: {
        Accept: "*/*",
        "Content-Type": "application/x-www-form-urlencoded"
        //  "X-Requested-With": "XMLHttpRequest"
      }
    };
    return axios
      .post(UPLOAD_ENDPOINT + "web/driver/destroy", formData, tipo)
      .then(response => {
        console.log(response);
        return response;
      })
      .catch(err => {
        console.log("dentro del upload", err.response);
        return err.response;
      });
  };
  return (
    <Grid container spacing={4}>
      <Grid item xs={12}>
        {" "}
        <Table className="mb-0">
          <TableHead>
            <TableRow>
              {table.map(key => (
                <TableCell key={key}>{key}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {datos.map(({ id, full_name, ci, mobile }) => (
              <TableRow key={id}>
                <TableCell className="pl-3 fw-normal">{id}</TableCell>
                <TableCell>{full_name}</TableCell>
                <TableCell>{ci}</TableCell>
                <TableCell>{mobile}</TableCell>
                <TableCell>
                  <Link to={"/app/transportista/edit/" + id}>
                    <IconButton aria-label="edit" color="primary">
                      <EditIcon />
                    </IconButton>
                  </Link>{" "}
                  <Link to={"/app/transportista/show/" + id}>
                    <IconButton aria-label="show" color="primary" size="small">
                      <VisibilityIcon />
                    </IconButton>
                  </Link>
                  <IconButton
                    aria-label="delete"
                    color="secondary"
                    onClick={e => handleClickOpenD(e, id)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>
      <Dialog
        open={openD}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleCloseD}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {"Esta seguro en eliminar?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Una vez eliminado no se podrá recuperar.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleCloseD} color="secondary">
            Cancelar
          </Button>
          <Button variant="contained" onClick={onSubmit} color="primary">
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
      {/* Verdadero */}
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        open={openV}
        autoHideDuration={4000}
        onClose={handleCloseV}
      >
        <Notification
          onClose={handleCloseV}
          variant="success"
          message="Eliminado Correctamente"
        />
      </Snackbar>
    </Grid>
  );
}
