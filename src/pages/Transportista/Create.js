//importacion
import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import {
  Grid,
  Button,
  TextField,
  InputLabel,
  Select,
  FormControl,
  Paper,
  IconButton,
  CircularProgress,
  Snackbar
} from "@material-ui/core";
import Notification from "../../components/Notification/Notification";
import "date-fns";
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from "@material-ui/pickers";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import { makeStyles } from "@material-ui/styles";
import { getItem } from "../../services/Transportista";
import PageTitle from "../../components/PageTitle/PageTitle";
import { useHistory } from "react-router-dom";
import { UPLOAD_ENDPOINT } from "../../config";

//estilos
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200
  },
  selectEmpty: {
    marginTop: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    color: theme.palette.primary.main
  }
}));
export default function RegistroTransportista() {
  const initialFormState = {
    id: "",
    nombre: "",
    ci: "",
    email: "",
    password: "",
    code: "591",
    mobile: "",
    telephone: "",
    imgPersonal: [null],
    namePersonal: "",
    imgBrevet: [null],
    nameBrevet: "",
    imgCarnet: [null],
    nameCarnet: "",
    account: "",
    departamento: "",
    address: "",
    owner: "",
    imgRuat: [null],
    nameRuat: "",
    imgSoat: [null],
    nameSoat: "",
    imgCar: [null],
    nameCar: "",
    plate: "",
    model: "",
    tipov: "",
    marca: "",
    color: ""
  };
  const [state, setState] = useState(initialFormState);
  const [isLoading, setIsLoading] = useState(false);
  const classes = useStyles();
  let history = useHistory();
  const [openV, setOpenV] = useState(false);
  const [openF, setOpenF] = useState(false);

  const [tipoVehiculos, setTipoVehiculos] = useState([]);
  const [marcas, setMarcas] = useState([]);
  const [colores, setColores] = useState([]);
  const [errores, setErrores] = useState([]);
  const [load, setLoad] = useState(false);
  const inputLabel = React.useRef("");
  const [labelWidth, setLabelWidth] = React.useState(0);

  // The first commit of Material-UI
  const [selectedDate, setSelectedDate] = React.useState(new Date());

  const handleDateChange = date => {
    setSelectedDate(
      date.getFullYear() +
        "-" +
        (date.getMonth() + 1) +
        "-" +
        date.getDate() +
        " " +
        date.getHours() +
        ":" +
        date.getMinutes() +
        ":" +
        date.getSeconds()
    );
  };
  // Consume the context store to access the `addContact` action

  useEffect(() => {
    const obtenerTipo = () => {
      getItem("https://webcargobo.com/api/web/type/index")
        .then(res => {
          setTipoVehiculos(res);
          setLabelWidth(inputLabel.current.offsetWidth);
          console.log(res);
          setLoad(true);
        })
        .catch(err => {
          console.log(err);
        });
    };
    const obtenerMarcas = () => {
      getItem("https://webcargobo.com/api/web/marks/index")
        .then(res => {
          setMarcas(res);
          setLabelWidth(inputLabel.current.offsetWidth);
          console.log("marcas", res);
        })
        .catch(err => {
          console.log(err);
        });
    };
    const obtenerColores = () => {
      getItem("https://webcargobo.com/api/web/colors/index")
        .then(res => {
          setColores(res);
          setLabelWidth(inputLabel.current.offsetWidth);
          console.log("colores", res);
        })
        .catch(err => {
          console.log(err);
        });
    };
    obtenerColores();
    obtenerMarcas();
    obtenerTipo();
  }, []);

  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, [tipoVehiculos]);

  //enviar datos al servidor
  const uploadFile = async (state, selectedDate) => {
    console.log(state, selectedDate);
    const formData = new FormData();
    formData.append("email", state.email);
    formData.append("full_name", state.nombre);
    formData.append("ci", state.ci);
    formData.append("birthday", selectedDate);
    formData.append("country_code", state.code);
    formData.append("mobile", state.mobile);
    formData.append("telephone", state.telephone);
    formData.append("number_account", state.account);
    formData.append("address", state.address);
    formData.append("department", state.departamento);
    formData.append("password", state.password);
    formData.append("car_owner", state.owner);
    formData.append("license_plate", state.plate);
    formData.append("mark_id", state.marca);
    formData.append("model", state.model);
    formData.append("color_id", state.color);
    formData.append("type_vehicle_id", state.tipov);

    formData.append("url_profile", state.imgPersonal);
    formData.append("image_carnet", state.imgCarnet);
    formData.append("image_brevet", state.imgBrevet);
    formData.append("image_ruat", state.imgRuat);
    formData.append("image_soat", state.imgSoat);
    formData.append("image_car", state.imgCar);
    const tipo = {
      headers: {
        Accept: "*/*",
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Requested-With": "XMLHttpRequest"
      }
    };
    return axios
      .post(UPLOAD_ENDPOINT + "web/driver/register", formData, tipo)
      .then(response => {
        console.log(response);
        return response;
      })
      .catch(err => {
        console.log("dentro del upload", err.response);
        return err.response;
      });
  };
  //Funciones
  const handleInputChange = event => {
    const { name, value } = event.target;
    setState({ ...state, [name]: value });
  };

  const handleCloseV = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenV(false);
  };
  const handleCloseF = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenF(false);
  };
  //Cambio imagen
  const onChange = e => {
    const global1 = e.target.files[0];
    setState(() => ({
      ...state,
      [e.target.id]: global1,
      [e.target.name]: e.target.files[0].name
    }));
  };

  const onSubmit = async e => {
    e.preventDefault();

    setIsLoading(true);
    return await uploadFile(state, selectedDate).then(res => {
      if (res.status === 200) {
        console.log("correcto", res.status);
        setIsLoading(false);
        setOpenV(true);
        history.push("/app/transportista");
      } else {
        console.log("errores", res.data);
        setIsLoading(false);
        setErrores(res.data);
        setOpenF(true);
      }
    });
  };

  if (load) {
    return (
      <Fragment>
        <Paper className={classes.paper}>
          <PageTitle
            title="Datos Transportista"
            button="Regresar"
            url="/app/transportista"
          />
          <form onSubmit={onSubmit}>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
              spacing={3}
            >
              {/* fila 1 inicio */}
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  label="Nombre Completo"
                  name="nombre"
                  type="text"
                  value={state.nombre}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="ci"
                  label="Carnet de Identidad"
                  type="text"
                  fullWidth
                  value={state.ci}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <MuiPickersUtilsProvider utils={DateFnsUtils}>
                  <KeyboardDatePicker
                    disableToolbar
                    variant="inline"
                    format={"yyyy/MM/dd"}
                    id="date-picker-inline"
                    label="Fecha Nacimiento"
                    value={selectedDate}
                    onChange={handleDateChange}
                    KeyboardButtonProps={{
                      "aria-label": "change date"
                    }}
                  />
                </MuiPickersUtilsProvider>
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="email"
                  label="email"
                  type="text"
                  fullWidth
                  value={state.email}
                  onChange={handleInputChange}
                />
              </Grid>
              {/* fila fin */}
              {/* fila 2 inicio */}
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  label="Contraseña"
                  name="password"
                  type="password"
                  value={state.password}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="code"
                  label="Codigo Pais"
                  type="number"
                  fullWidth
                  value={state.code}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="mobile"
                  label="Celular"
                  type="number"
                  fullWidth
                  value={state.mobile}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="telephone"
                  label="Teléfono"
                  type="text"
                  fullWidth
                  value={state.telephone}
                  onChange={handleInputChange}
                />
              </Grid>
              {/* fila fin */}
              {/* fila 3 inicio */}
              <Grid item xs={12} sm={3}>
                <input
                  style={{ display: "none" }}
                  accept="image/*"
                  id="imgPersonal"
                  type="file"
                  name="namePersonal"
                  onChange={onChange}
                />
                <label htmlFor="imgPersonal">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
                <TextField
                  label="Foto Personal"
                  disabled
                  type="text"
                  value={state.namePersonal || ""}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <input
                  style={{ display: "none" }}
                  accept="image/*"
                  id="imgBrevet"
                  type="file"
                  name="nameBrevet"
                  onChange={onChange}
                />
                <label htmlFor="imgBrevet">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
                <TextField
                  label="Foto Brevet"
                  disabled
                  type="text"
                  value={state.nameBrevet || ""}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <input
                  style={{ display: "none" }}
                  accept="image/*"
                  id="imgCarnet"
                  type="file"
                  name="nameCarnet"
                  onChange={onChange}
                />
                <label htmlFor="imgCarnet">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
                <TextField
                  label="Foto Carnet Identidad"
                  disabled
                  type="text"
                  value={state.nameCarnet || ""}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  fullWidth
                  label="Numero de Cuenta"
                  name="account"
                  type="text"
                  required
                  value={state.account}
                  onChange={handleInputChange}
                />
              </Grid>
              {/* fila fin */}
              {/* fila 4 inicio */}
              <Grid item xs={12} sm={3}>
                <FormControl>
                  <InputLabel ref={inputLabel} htmlFor="departamento">
                    Departamento
                  </InputLabel>
                  <Select
                    native
                    value={state.departamento}
                    onChange={handleInputChange}
                    labelWidth={labelWidth}
                    inputProps={{
                      name: "departamento",
                      id: "departamento"
                    }}
                  >
                    <option value="" />
                    <option key={1} value={"Beni"}>
                      {" "}
                      Beni{" "}
                    </option>
                    <option key={2} value={"Chuquisaca"}>
                      {" "}
                      Chuquisaca{" "}
                    </option>
                    <option key={3} value={"Cochabamba"}>
                      {" "}
                      Cochabamba{" "}
                    </option>
                    <option key={4} value={"La Paz"}>
                      {" "}
                      La Paz{" "}
                    </option>
                    <option key={5} value={"Oruro"}>
                      {" "}
                      Oruro{" "}
                    </option>
                    <option key={6} value={"Pando"}>
                      {" "}
                      Pando{" "}
                    </option>
                    <option key={7} value={"Potosí"}>
                      {" "}
                      Potosí{" "}
                    </option>
                    <option key={8} value={"Santa Cruz"}>
                      {" "}
                      Santa Cruz{" "}
                    </option>
                    <option key={9} value={"Tarija"}>
                      {" "}
                      Tarija{" "}
                    </option>
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  name="address"
                  label="Direccion"
                  type="text"
                  fullWidth
                  value={state.address}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}></Grid>
              {/* fila fin */}
              <Grid item xs={12} sm={12}>
                <PageTitle title="Datos Vehiculo" />
              </Grid>
              {/* fila 5 inicio */}
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  label="Nombre Propietario"
                  name="owner"
                  type="text"
                  value={state.owner}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <input
                  style={{ display: "none" }}
                  accept="image/*"
                  id="imgRuat"
                  type="file"
                  name="nameRuat"
                  onChange={onChange}
                />
                <label htmlFor="imgRuat">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
                <TextField
                  label="Foto Ruat"
                  disabled
                  type="text"
                  value={state.nameRuat || ""}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <input
                  style={{ display: "none" }}
                  accept="image/*"
                  id="imgSoat"
                  type="file"
                  name="nameSoat"
                  onChange={onChange}
                />
                <label htmlFor="imgSoat">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
                <TextField
                  label="Foto Soat"
                  disabled
                  type="text"
                  value={state.nameSoat || ""}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <input
                  style={{ display: "none" }}
                  accept="image/*"
                  id="imgCar"
                  type="file"
                  name="nameCar"
                  onChange={onChange}
                />
                <label htmlFor="imgCar">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
                <TextField
                  label="Foto Vehiculo"
                  disabled
                  type="text"
                  value={state.nameCar || ""}
                />
              </Grid>
              {/* fila fin */}
              {/* fila 6 inicio */}
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  label="Numero de Placa"
                  name="plate"
                  type="text"
                  value={state.plate}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  name="model"
                  label="Modelo"
                  type="text"
                  fullWidth
                  value={state.model}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <FormControl>
                  <InputLabel ref={inputLabel} htmlFor="tipov">
                    Tipo Vehiculo
                  </InputLabel>
                  <Select
                    native
                    value={state.tipov}
                    onChange={handleInputChange}
                    labelWidth={labelWidth}
                    inputProps={{
                      name: "tipov",
                      id: "tipov"
                    }}
                  >
                    <option value="" />
                    {tipoVehiculos.map(ocup => {
                      return (
                        <option key={ocup.id} value={ocup.id}>
                          {ocup.name}{" "}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={3}>
                <FormControl>
                  <InputLabel ref={inputLabel} htmlFor="marca">
                    Marca
                  </InputLabel>
                  <Select
                    native
                    value={state.marca}
                    onChange={handleInputChange}
                    labelWidth={labelWidth}
                    inputProps={{
                      name: "marca",
                      id: "marca"
                    }}
                  >
                    <option value="" />

                    {marcas.map(mar => {
                      return (
                        <option key={mar.id} value={mar.id}>
                          {mar.name}{" "}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>

              <Grid item xs={12} sm={3}>
                <FormControl>
                  <InputLabel ref={inputLabel} htmlFor="color">
                    Color
                  </InputLabel>
                  <Select
                    native
                    value={state.color}
                    onChange={handleInputChange}
                    labelWidth={labelWidth}
                    inputProps={{
                      name: "color",
                      id: "color"
                    }}
                  >
                    <option value="" />
                    {colores.map(col => {
                      return (
                        <option key={col.id} value={col.id}>
                          {col.name}{" "}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              {/* fila fin */}
              <Grid item xs={12} sm={12}>
                <div className={classes.creatingButtonContainer}>
                  {isLoading ? (
                    <CircularProgress size={26} />
                  ) : (
                    <Button
                      onClick={onSubmit}
                      variant="contained"
                      color="primary"
                      className={classes.createAccountButton}
                    >
                      Guardar
                    </Button>
                  )}
                </div>
              </Grid>
            </Grid>
          </form>
        </Paper>
        {/* Falso */}
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          open={openF}
          autoHideDuration={4000}
          onClose={handleCloseF}
        >
          <Notification
            onClose={handleCloseF}
            variant="error"
            message={
              `Completar campos Faltantes: 
             ` + errores
            }
          />
        </Snackbar>
        {/* Verdadero */}
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          open={openV}
          autoHideDuration={4000}
          onClose={handleCloseV}
        >
          <Notification
            onClose={handleCloseF}
            variant="success"
            message="Creado"
          />
        </Snackbar>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
