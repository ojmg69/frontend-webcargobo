import React, { useState } from "react";
import Copyright from "../../components/Copyright";
import { login } from "../../services/UserFunctions";
import {
  CircularProgress,
  Typography,
  Button,
  TextField,
  Fade,
  Avatar,
  CssBaseline,
  Box,
  Container
} from "@material-ui/core";
import { withRouter } from "react-router-dom";

// styles
import useStyles from "./styles";

// logo
import logo from "../../images/logo.png";

// context
import { useUserDispatch, loginUser } from "../../context/UserContext";

function Login(props) {
  var classes = useStyles();

  // global
  var userDispatch = useUserDispatch();

  // local
  var [isLoading, setIsLoading] = useState(false);
  var [error, setError] = useState(null);
  var [loginValue, setLoginValue] = useState("");
  var [passwordValue, setPasswordValue] = useState("");

  var [email, setEmail] = useState("");
  var [password, setPassword] = useState("");
  var [cargar, setCargar] = useState(false);
  var [errores, setErrores] = useState({});

  /*const handleForm = e => {
    setCargar(true);
    setErrores({});
    e.preventDefault();
    axios
      .post("http://localhost:8000/api/auth/login", { email, password })
      .then(res => {
        //cookie.set("token", res.data.access_token);
        //localStorage.setItem('id_user',2);
        //this.props.setLogin(res.data.user);
        console.log(res);
        props.history.replace("/dashboard");
        setCargar(false);
      })
      .catch(e => {
        setCargar(false);
        setErrores(e.response.data);
        console.log(e.response.data.errors);
      });
  };*/

  const handleForm = e => {
    setCargar(true);
    setErrores({});
    e.preventDefault();
    const user = {
      email: email,
      password: password
    };
    login(user).then(res => {
      if (res) {
        console.log(res);
        props.history.push(`/dashboard`);
      } else {
        setCargar(false);
        setErrores({ errors: "Verifique su usuario o contraseña" });
      }
    });
  };
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar
          variant="square"
          alt="Webcargo"
          src={logo}
          className={classes.bigAvatar}
        />

        <form className={classes.form} onSubmit={handleForm}>
          <Fade in={error}>
            <Typography color="secondary" className={classes.errorMessage}>
              Something is wrong with your login or password :(
            </Typography>
          </Fade>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Correo Electronico"
            name="email"
            autoComplete="email"
            autoFocus
            value={loginValue}
            onChange={e => setLoginValue(e.target.value)}
            placeholder="Email Adress"
            type="email"
          />
          <TextField
            variant="outlined"
            required
            name="password"
            label="Contraseña"
            id="password"
            autoComplete="current-password"
            value={passwordValue}
            onChange={e => setPasswordValue(e.target.value)}
            margin="normal"
            placeholder="Password"
            type="password"
            fullWidth
          />
          <div className={classes.creatingButtonContainer}>
            {isLoading ? (
              <CircularProgress size={26} />
            ) : (
              <Button
                onClick={() =>
                  loginUser(
                    userDispatch,
                    loginValue,
                    passwordValue,
                    props.history,
                    setIsLoading,
                    setError
                  )
                }
                disabled={loginValue.length === 0 || passwordValue.length === 0}
                size="large"
                variant="contained"
                color="primary"
                fullWidth
                className={classes.createAccountButton}
              >
                Ingresar
              </Button>
            )}
          </div>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
export default withRouter(Login);
