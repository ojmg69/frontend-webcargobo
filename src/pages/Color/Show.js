import React, { useEffect, useState, Fragment } from "react";
import { Paper } from "@material-ui/core";
import { UPLOAD_ENDPOINT } from "../../config";
// styles
import useStyles from "./styles";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
// components
import PageTitle from "../../components/PageTitle/PageTitle";

// icons sets
import "font-awesome/css/font-awesome.min.css";
import axios from "axios";
function View(props) {
  const [datos, setDatos] = useState();
  const [load, setLoad] = useState(false);
  var classes = useStyles();
  useEffect(() => {
    const { id } = props.match.params;
    const editando = async () => {
      const formData = new FormData();
      formData.append("id", id);
      const tipo = {
        headers: {
          Accept: "*/*",
          "Content-Type": "application/json"
        }
      };
      try {
        const response = await axios.post(
          UPLOAD_ENDPOINT + "web/colors/show",
          formData,
          tipo
        );
        console.log(response.data);
        setDatos(response.data);
        setLoad(true);
      } catch (err) {
        console.log("dentro del upload", err.response);
      }
    };
    editando();
  }, [props.match.params]);
  if (load) {
    return (
      <Fragment>
        <Paper className={classes.iconsContainer}>
          <Grid container spacing={3}>
            <Grid item xs={12} sm={12}>
              <PageTitle
                title="Detalle Color"
                button="Regresar"
                url="/app/color"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component="h6" variant="h6" display="inline">
                Nombre :
              </Typography>
              {datos[0].name}
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Creado / Modificado:
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].admin}
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
export default View;
