import React, { useEffect, useState, Fragment } from "react";
import { UPLOAD_ENDPOINT } from "../../config";
import { Paper } from "@material-ui/core";

// styles
import useStyles from "./styles";

// components
import PageTitle from "../../components/PageTitle/PageTitle";
import List from "./List";

// icons sets
import "font-awesome/css/font-awesome.min.css";
import axios from "axios";

function MarkPage() {
  const [datos, setDatos] = useState([]);
  const [load, setLoad] = useState(false);
  var classes = useStyles();
  useEffect(() => {
    axios
      .get(UPLOAD_ENDPOINT + "web/colors/index")
      .then(res => {
        setDatos(res.data);
        setLoad(true);
      })
      .catch(err => {
        // setError(err.message);
        setLoad(true);
      });
  }, []);
  if (load) {
    return (
      <Fragment>
        <PageTitle
          title="Gestionar Colores"
          button="Crear"
          url="/app/color/add"
        />
        <Paper className={classes.iconsContainer}>
          <List datos={datos} />
        </Paper>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
export default MarkPage;
