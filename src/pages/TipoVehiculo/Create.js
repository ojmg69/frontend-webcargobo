//importaciones
import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import Notification from "../../components/Notification/Notification";
import {
  Grid,
  Button,
  TextField,
  InputLabel,
  Select,
  FormControl,
  Paper,
  IconButton,
  CircularProgress,
  Snackbar
} from "@material-ui/core";
import PhotoCamera from "@material-ui/icons/PhotoCamera";
import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/styles";
import { amber, green } from "@material-ui/core/colors";
import { getItem } from "../../services/Transportista";
import PageTitle from "../../components/PageTitle/PageTitle";

//Declaracion de Variables
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200
  },
  selectEmpty: {
    marginTop: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    color: theme.palette.primary.main
  },
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  info: {
    backgroundColor: theme.palette.primary.main
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
}));

function ContactForm() {
  //hooks
  const [openV, setOpenV] = useState(false);
  const [openF, setOpenF] = useState(false);
  let history = useHistory();
  const classes = useStyles();
  const [ocupaciones, setOcupaciones] = useState([]);
  const initialFormState = {
    id: "",
    nombre: "",
    description: "",
    iimagen: [null],
    nimagen: "",
    category: "",
    precio: "",
    distanceMin: "",
    distanceMax: "",
    costMin: "",
    costMax: "",
    commission: "",
    tnCost: ""
  };
  const UPLOAD_ENDPOINT = "https://webcargobo.com/api/";
  const [state, setState] = useState(initialFormState);
  const inputLabel = React.useRef("");
  const [labelWidth, setLabelWidth] = React.useState(0);
  const [isLoading, setIsLoading] = useState(false);
  const [load, setLoad] = useState(false);

  const handleCloseV = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenV(false);
  };
  const handleCloseF = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenF(false);
  };

  //Funciones
  const handleInputChange = event => {
    const { name, value } = event.target;
    setState({ ...state, [name]: value });
  };

  //enviar datos al servidor
  const uploadFile = async state => {
    const formData = new FormData();
    formData.append("name", state.nombre);
    formData.append("description", state.description);
    formData.append("url_image", state.iimagen);
    formData.append("category_id", state.category);
    formData.append("base_price", state.precio);
    formData.append("min_distance", state.distanceMin);
    formData.append("max_distance", state.distanceMax);
    formData.append("min_cost_km", state.costMin);
    formData.append("max_cost_km", state.costMax);
    formData.append("commission_percentage", state.commission);
    formData.append("tn_cost", state.tnCost);
    const tipo = {
      headers: {
        Accept: "*/*",
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Requested-With": "XMLHttpRequest"
      }
    };
    return axios
      .post(UPLOAD_ENDPOINT + "web/type/add", formData, tipo)
      .then(response => {
        console.log(response);
        return response;
      })
      .catch(err => {
        console.log("dentro del upload", err.response);
        return err.response;
      });
  };
  //Cambio imagen
  const onChange = e => {
    const global1 = e.target.files[0];
    setState(() => ({
      ...state,
      [e.target.id]: global1,
      [e.target.name]: e.target.files[0].name
    }));
  };

  useEffect(() => {
    const obtener = () => {
      getItem("https://webcargobo.com/api/web/categories/index")
        .then(res => {
          setOcupaciones(res);

          setLabelWidth(inputLabel.current.offsetWidth);
          setLoad(true);
        })
        .catch(err => {
          console.log(err);
        });
    };

    obtener();
  }, []);
  React.useEffect(() => {
    setLabelWidth(inputLabel.current.offsetWidth);
  }, [labelWidth]);
  //enviar formulario
  const onSubmit = async e => {
    e.preventDefault();
    console.log(state);
    setIsLoading(true);
    return await uploadFile(state).then(res => {
      if (res.status === 200) {
        console.log("correcto", res.status);
        setIsLoading(false);
        setOpenV(true);
        history.push("/app/tipo");
      } else {
        console.log("errores", res.data);
        setIsLoading(false);
        setOpenF(true);
      }
    });
  };

  if (load) {
    return (
      <Fragment>
        <Paper className={classes.paper}>
          <PageTitle title="Registrar Tipo" button="Regresar" url="/app/tipo" />
          <form onSubmit={onSubmit}>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
              spacing={3}
            >
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  label="Nombre "
                  name="nombre"
                  type="text"
                  value={state.nombre}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  required
                  fullWidth
                  type="text"
                  value={state.description}
                  onChange={handleInputChange}
                  name="description"
                  label="Descripción"
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <input
                  style={{ display: "none" }}
                  accept="image/*"
                  id="iimagen"
                  type="file"
                  name="nimagen"
                  onChange={onChange}
                />
                <label htmlFor="iimagen">
                  <IconButton
                    color="primary"
                    aria-label="upload picture"
                    component="span"
                  >
                    <PhotoCamera />
                  </IconButton>
                </label>
                <TextField disabled type="text" value={state.nimagen || ""} />
              </Grid>
              <Grid item xs={12} sm={3}>
                <FormControl>
                  <InputLabel
                    ref={inputLabel}
                    htmlFor="outlined-age-native-simple"
                  >
                    Categoria
                  </InputLabel>
                  <Select
                    native
                    value={state.category}
                    onChange={handleInputChange}
                    labelWidth={labelWidth}
                    inputProps={{
                      name: "category",
                      id: "outlined-age-native-simple"
                    }}
                  >
                    <option value="" />
                    {ocupaciones.map(ocup => {
                      return (
                        <option key={ocup.id} value={ocup.id}>
                          {ocup.name}{" "}
                        </option>
                      );
                    })}
                  </Select>
                </FormControl>
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  type="number"
                  value={state.precio}
                  onChange={handleInputChange}
                  name="precio"
                  label="Precio Base"
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  type="number"
                  value={state.distanceMin}
                  onChange={handleInputChange}
                  name="distanceMin"
                  label="Distancia Minima"
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  type="number"
                  value={state.distanceMax}
                  onChange={handleInputChange}
                  name="distanceMax"
                  label="Distancia Máxima"
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  type="number"
                  value={state.costMin}
                  onChange={handleInputChange}
                  name="costMin"
                  label="Costo Minimo por KM"
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  type="number"
                  value={state.costMax}
                  onChange={handleInputChange}
                  name="costMax"
                  label="Costo Máximo por KM"
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  type="number"
                  value={state.commission}
                  onChange={handleInputChange}
                  name="commission"
                  label="Porcentaje"
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  fullWidth
                  type="number"
                  value={state.tnCost}
                  onChange={handleInputChange}
                  name="tnCost"
                  label="Costo por Tonelaje"
                  helperText="Introducir 0 si es de categoria Liviano"
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <div className={classes.creatingButtonContainer}>
                  {isLoading ? (
                    <CircularProgress size={26} />
                  ) : (
                    <Button
                      onClick={onSubmit}
                      variant="contained"
                      color="primary"
                      className={classes.createAccountButton}
                    >
                      Guardar
                    </Button>
                  )}
                </div>
              </Grid>
            </Grid>
          </form>
        </Paper>
        {/* Falso */}
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          open={openF}
          autoHideDuration={4000}
          onClose={handleCloseF}
        >
          <Notification
            onClose={handleCloseF}
            variant="error"
            message="Completar campos Faltantes"
          />
        </Snackbar>
        {/* Verdadero */}
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          open={openV}
          autoHideDuration={4000}
          onClose={handleCloseV}
        >
          <Notification
            onClose={handleCloseF}
            variant="success"
            message="Creado"
          />
        </Snackbar>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
export default ContactForm;
