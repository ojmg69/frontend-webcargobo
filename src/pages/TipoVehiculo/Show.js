import React, { useEffect, useState, Fragment } from "react";
import { Paper } from "@material-ui/core";
import { UPLOAD_ENDPOINT, IMAGES } from "../../config";
// styles
import useStyles from "./styles";

import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
// components
import PageTitle from "../../components/PageTitle/PageTitle";

// icons sets
import "font-awesome/css/font-awesome.min.css";
import axios from "axios";
function View(props) {
  const [datos, setDatos] = useState();
  const [load, setLoad] = useState(false);
  var classes = useStyles();
  useEffect(() => {
    const { id } = props.match.params;
    const editando = async () => {
      const formData = new FormData();
      formData.append("id", id);
      const tipo = {
        headers: {
          Accept: "*/*",
          "Content-Type": "application/x-www-form-urlencoded",
          "X-Requested-With": "XMLHttpRequest"
        }
      };
      try {
        const response = await axios.post(
          UPLOAD_ENDPOINT + "web/type/show",
          formData,
          tipo
        );
        console.log(response.data);
        setDatos(response.data);
        setLoad(true);
      } catch (err) {
        console.log("dentro del upload", err.response);
      }
    };
    editando();
  }, [props.match.params]);
  if (load) {
    return (
      <Fragment>
        <PageTitle
          title="Detalle Tipo Vehiculo y Precio"
          button="Regresar"
          url="/app/tipo"
        />
        <Paper className={classes.iconsContainer}>
          <Grid container spacing={3}>
            <Grid item xs={12}>
              <Typography component="h6" variant="h6" display="inline">
                Imagen :
              </Typography>
            </Grid>

            <Grid item xs={12}>
              <img
                src={IMAGES + datos[0].url_image}
                alt=""
                height="150"
                width="150"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component="h6" variant="h6" display="inline">
                Nombre :
              </Typography>
              {datos[0].name}
              <Typography
                component="p"
                variant="subtitle1"
                display="inline"
              ></Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Descripción :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].description}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Categoria :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].categoria}
              </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Precio Base :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].base_price}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Distancia Mínima :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].min_distance}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Distancia Maxima :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].max_distance}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Costo Mínimo :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].min_cost_km}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Costo Máximo :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].max_cost_km}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Porcentaje Comisión :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].commission_percentage}
              </Typography>
            </Grid>

            <Grid item xs={12} sm={6}>
              <Typography component="p" variant="h6" display="inline">
                Costo Por Tonelaje :
              </Typography>
              <Typography component="p" variant="subtitle1" display="inline">
                {datos[0].tn_cost}
              </Typography>
            </Grid>
          </Grid>
        </Paper>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
export default View;
