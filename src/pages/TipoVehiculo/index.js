import React, { useEffect, useState, Fragment } from "react";
import { Paper } from "@material-ui/core";
// styles
import useStyles from "./styles";
// components
import PageTitle from "../../components/PageTitle/PageTitle";
import List from "./List";
// icons sets
import "font-awesome/css/font-awesome.min.css";
import axios from "axios";
function IconsPage(props) {
  const [datos, setDatos] = useState([]);
  const [load, setLoad] = useState(false);
  var classes = useStyles();
  useEffect(() => {
    axios
      .get("https://webcargobo.com/api/web/type/index")
      .then(res => {
        setDatos(res.data);
        setLoad(true);
      })
      .catch(err => {
        // setError(err.message);
        setLoad(true);
      });
  }, []);
  if (load) {
    return (
      <Fragment>
        <PageTitle
          title="Tipo y Precio Vehiculo"
          button="Crear"
          url="/app/tipo/add"
        />
        <Paper className={classes.iconsContainer}>
          <List datos={datos} />
        </Paper>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
export default IconsPage;
