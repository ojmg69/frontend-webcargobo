//Importaciones
import React, { useState } from "react";

import {
  Grid,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell,
  IconButton,
  Snackbar,
  CircularProgress
} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import axios from "axios";
import Notification from "../../components/Notification/Notification";
import { UPLOAD_ENDPOINT } from "../../config";

import { Link } from "react-router-dom";
import { Edit as EditIcon, Delete as DeleteIcon } from "@material-ui/icons";
import VisibilityIcon from "@material-ui/icons/Visibility";
import { useHistory } from "react-router-dom";
const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});
export default function List(props) {
  // Declare a local state to be used internally by this component
  const { datos } = props;
  const URL = "https://webcargobo.com/storage/";
  const table = [
    "#",
    "Nombre",
    "Descripción",
    "Categoria",
    "Imagen",
    "Acciones"
  ];
  const [openD, setOpenD] = React.useState(false);
  const [idU, setIdU] = React.useState("");
  const [openV, setOpenV] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  let history = useHistory();

  const handleClickOpenD = (e, id) => {
    setOpenD(true);
    setIdU(id);

    console.log(id);
  };

  const handleCloseD = () => {
    setOpenD(false);
  };
  //enviar formulario
  const onSubmit = async e => {
    setOpenD(false);
    e.preventDefault();
    console.log(idU);
    setIsLoading(true);

    return await uploadFile(idU).then(res => {
      if (res.status === 200) {
        console.log("Eliminado Correctamente", res.status);
        setIsLoading(false);
        setOpenV(true);
      }
      history.push("/app/dashboard");
      //window.location.reload(false);
    });
  };
  const handleCloseV = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenV(false);
  };
  //enviar datos al servidor
  const uploadFile = async idU => {
    const formData = new FormData();
    formData.append("id", idU);
    const tipo = {
      headers: {
        Accept: "*/*",
        "Content-Type": "application/x-www-form-urlencoded",
        "X-Requested-With": "XMLHttpRequest"
      }
    };
    return axios
      .post(UPLOAD_ENDPOINT + "web/type/destroy", formData, tipo)
      .then(response => {
        console.log(response);
        return response;
      })
      .catch(err => {
        console.log("dentro del upload", err.response);
        return err.response;
      });
  };
  return (
    <Grid container spacing={4}>
      {isLoading ? <CircularProgress size={26} /> : <div></div>}
      <Grid item xs={12}>
        <Table className="mb-0">
          <TableHead>
            <TableRow>
              {table.map(key => (
                <TableCell key={key}>{key}</TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {datos.map(({ id, name, description, categoria, url_image }) => (
              <TableRow key={id}>
                <TableCell className="pl-3 fw-normal">{id}</TableCell>
                <TableCell>{name}</TableCell>
                <TableCell>{description}</TableCell>
                <TableCell>{categoria}</TableCell>
                <TableCell>
                  <img src={URL + url_image} alt="" height="42" width="42" />
                </TableCell>
                <TableCell>
                  <Link to={"/app/tipo/edit/" + id}>
                    <IconButton aria-label="edit" color="primary">
                      <EditIcon />
                    </IconButton>
                  </Link>{" "}
                  <Link to={"/app/tipo/show/" + id}>
                    <IconButton aria-label="show" color="primary" size="small">
                      <VisibilityIcon />
                    </IconButton>
                  </Link>
                  <IconButton
                    aria-label="delete"
                    color="secondary"
                    onClick={e => handleClickOpenD(e, id)}
                  >
                    <DeleteIcon />
                  </IconButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </Grid>
      <Dialog
        open={openD}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleCloseD}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {"Esta seguro en eliminar?"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            Una vez eliminado no se podrá recuperar.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button variant="contained" onClick={handleCloseD} color="secondary">
            Cancelar
          </Button>
          <Button variant="contained" onClick={onSubmit} color="primary">
            Aceptar
          </Button>
        </DialogActions>
      </Dialog>
      {/* Verdadero */}
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        open={openV}
        autoHideDuration={4000}
        onClose={handleCloseV}
      >
        <Notification
          onClose={handleCloseV}
          variant="success"
          message="Eliminado Correctamente"
        />
      </Snackbar>
    </Grid>
  );
}
