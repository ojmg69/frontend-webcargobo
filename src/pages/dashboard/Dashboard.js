import React, { useEffect, useState } from "react";
import { Grid, Box, Paper } from "@material-ui/core";
import axios from "axios";
import { getItem } from "../../services/Transportista";
import { UPLOAD_ENDPOINT } from "../../config";
// styles
import useStyles from "./styles";

// components
import PageTitle from "../../components/PageTitle/PageTitle";
import Clients from "./components/Clients";
import Drivers from "./components/Drivers";
import Earnings from "./components/Earnings";
import Admin from "./components/Administrador";
import Copyright from "../../components/Copyright";

export default function Dashboard() {
  var classes = useStyles();
  const [usuarios, setUsuarios] = useState([]);
  const [transportistas, setTransportistas] = useState([]);
  const [datosAdmin, setDatosAdmin] = useState([]);
  //const [load, setLoad] = useState(false);
  useEffect(() => {
    const obtenerTotalConductor = () => {
      getItem("https://webcargobo.com/api/web/options/total_drivers")
        .then(res => {
          setTransportistas(res);
          console.log("drivers", res);
          // setLoad(true);
        })
        .catch(err => {
          console.log(err);
        });
    };
    const obtenerTotalClientes = () => {
      getItem("https://webcargobo.com/api/web/options/total_clients")
        .then(res => {
          setUsuarios(res);
          console.log("clientes", res);
        })
        .catch(err => {
          console.log(err);
        });
    };
    const id = localStorage.getItem("id");
    const obtenerAdmin = async () => {
      const formData = new FormData();
      formData.append("id", id);
      const tipo = {
        headers: {
          Accept: "*/*",
          "Content-Type": "application/json"
        }
      };
      try {
        const response = await axios.post(
          UPLOAD_ENDPOINT + "user",
          formData,
          tipo
        );
        const gl = response;
        console.log("datosAdmin", gl.data);
        setDatosAdmin(gl.data);
        //setLoad(true);
      } catch (err) {
        console.log("dentro del upload Editar", err.response);
      }
    };
    obtenerAdmin();
    obtenerTotalClientes();
    obtenerTotalConductor();
  }, []);

  return (
    <>
      <PageTitle title="Panel" />
      <Grid container spacing={3}>
        <Grid item lg={4} md={8} sm={6} xs={12}>
          <Paper className={classes.paper}>
            <Clients totalU={usuarios} />
          </Paper>
        </Grid>
        <Grid item lg={4} md={8} sm={6} xs={12}>
          <Paper className={classes.paper}>
            <Drivers totalT={transportistas} />
          </Paper>
        </Grid>
        <Grid item lg={4} md={8} sm={6} xs={12}>
          <Paper className={classes.paper}>
            <Earnings />
          </Paper>
        </Grid>
        {/* Datos Administrador*/}
        <Grid item xs={12}>
          <Paper className={classes.paper}>
            <Admin datosA={datosAdmin} />
          </Paper>
        </Grid>
      </Grid>
      <Box pt={4}>
        <Copyright />
      </Box>
    </>
  );
}

// #######################################################################
