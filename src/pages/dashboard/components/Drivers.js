import React from "react";
import { Link } from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import Title from "./Title";

export default function Clients(props) {
  return (
    <React.Fragment>
      <Title>Transportistas registrados</Title>
      <Typography component="p" variant="h6">
        Total: {props.totalT}
      </Typography>
      <div>
        <Link color="primary" to="/app/transportista">
          Ver Transportistas
        </Link>
      </div>
    </React.Fragment>
  );
}
