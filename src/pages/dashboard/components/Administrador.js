import React from "react";
import Title from "./Title";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
export default function Administrators(props) {
  const { datosA } = props;
  return (
    <React.Fragment>
      <Title>Datos de Personal</Title>

      <Grid container spacing={1}>
        <Grid item xs={12} sm={6}>
          <Typography component="h6" variant="h6" display="inline">
            Nombre y Apellidos :
          </Typography>
          {datosA.full_name}
          <Typography
            component="p"
            variant="subtitle1"
            display="inline"
          ></Typography>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Typography component="p" variant="h6" display="inline">
            Carnet de identidad :
          </Typography>
          <Typography component="p" variant="subtitle1" display="inline">
            {datosA.ci}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Typography component="p" variant="h6" display="inline">
            Fecha de nacimiento :
          </Typography>
          <Typography component="p" variant="subtitle1" display="inline">
            {datosA.birthday}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography component="p" variant="h6" display="inline">
            Celular :
          </Typography>
          <Typography component="p" variant="subtitle1" display="inline">
            {datosA.mobile}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <Typography component="p" variant="h6" display="inline">
            Telefono :
          </Typography>
          <Typography component="p" variant="subtitle1" display="inline">
            {datosA.telephone}
          </Typography>
        </Grid>

        <Grid item xs={12} sm={6}>
          <Typography component="p" variant="h6" display="inline">
            Email :
          </Typography>
          <Typography component="p" variant="subtitle1" display="inline">
            {datosA.email}
          </Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}
