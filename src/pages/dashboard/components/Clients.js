import React from "react";
import Link from "@material-ui/core/Link";
import Typography from "@material-ui/core/Typography";
import Title from "./Title";

function preventDefault(event) {
  event.preventDefault();
}

/*const useStyles = makeStyles({
  depositContext: {
    flex: 1,
  },
});*/

export default function Clients(props) {
  //const classes = useStyles();
  return (
    <React.Fragment>
      <Title>Clientes registrados</Title>
      <Typography component="p" variant="h6">
        Total: {props.totalU}
      </Typography>
      <div>
        <Link color="primary" href="#" onClick={preventDefault}>
          Ver Clientes
        </Link>
      </div>
    </React.Fragment>
  );
}
