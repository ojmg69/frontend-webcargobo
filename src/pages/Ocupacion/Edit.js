import React, { useState, useEffect, Fragment } from "react";
import axios from "axios";
import Notification from "../../components/Notification/Notification";
import {
  Grid,
  Button,
  TextField,
  Paper,
  CircularProgress,
  Snackbar
} from "@material-ui/core";

import { useHistory } from "react-router-dom";
import { makeStyles } from "@material-ui/styles";
import { amber, green } from "@material-ui/core/colors";
import PageTitle from "../../components/PageTitle/PageTitle";
import { UPLOAD_ENDPOINT } from "../../config";
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200
  },
  selectEmpty: {
    marginTop: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    color: theme.palette.primary.main
  },
  success: {
    backgroundColor: green[600]
  },
  error: {
    backgroundColor: theme.palette.error.dark
  },
  info: {
    backgroundColor: theme.palette.primary.main
  },
  warning: {
    backgroundColor: amber[700]
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing(1)
  },
  message: {
    display: "flex",
    alignItems: "center"
  }
}));

function ContactForm(props) {
  // Varibles
  const [openV, setOpenV] = useState(false);
  const [openF, setOpenF] = useState(false);
  let history = useHistory();
  const classes = useStyles();

  const [isLoading, setIsLoading] = useState(false);
  const [load, setLoad] = useState(false);
  const initialFormState = {
    id: "",
    name: "",
    userId: ""
  };
  const [state, setState] = useState(initialFormState);

  //funciones
  const handleCloseV = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenV(false);
  };
  const handleCloseF = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenF(false);
  };

  //Funciones
  const handleInputChange = event => {
    const { name, value } = event.target;
    setState(() => ({
      ...state,
      [name]: value,
      userId: localStorage.getItem("id")
    }));
  };
  //enviar datos al servidor
  const uploadFile = async state => {
    const formData = new FormData();
    formData.append("id", state.id);
    formData.append("name", state.name);
    formData.append("user_id", state.userId);

    const tipo = {
      headers: {
        Accept: "*/*",
        "Content-Type": "application/json"
      }
    };
    return axios
      .post(UPLOAD_ENDPOINT + "web/occupations/update", formData, tipo)
      .then(response => {
        console.log(response);
        return response;
      })
      .catch(err => {
        console.log("dentro del upload EDIT", err);
        return err.response;
      });
  };

  useEffect(() => {
    const { id } = props.match.params;
    const editando = async () => {
      const formData = new FormData();
      formData.append("id", id);
      const tipo = {
        headers: {
          Accept: "*/*",
          "Content-Type": "application/json"
        }
      };
      try {
        const response = await axios.post(
          UPLOAD_ENDPOINT + "web/occupations/show",
          formData,
          tipo
        );

        const gl = response.data[0];
        setState(() => ({
          id: gl.id,
          name: gl.name,
          userId: gl.user_id
        }));
        setLoad(true);
      } catch (err) {
        console.log("dentro del upload Editar", err.response);
      }
    };
    editando();
  }, []);

  //enviar formulario
  const onSubmit = async e => {
    e.preventDefault();

    setIsLoading(true);
    return await uploadFile(state).then(res => {
      if (res.status === 200) {
        console.log("correcto", res.data);
        setIsLoading(false);
        setOpenV(true);
        history.push("/app/ocupacion");
      } else {
        console.log("errores", res.data);
        setIsLoading(false);
        setOpenF(true);
      }
    });
  };

  if (load) {
    return (
      <Fragment>
        <Paper className={classes.paper}>
          <PageTitle
            title="Modificar Ocupación"
            button="Regresar"
            url="/app/ocupacion"
          />
          <form onSubmit={onSubmit}>
            <Grid
              container
              direction="row"
              justify="flex-start"
              alignItems="flex-start"
              spacing={3}
            >
              {/* fila 1 inicio */}
              <Grid item xs={12} sm={3}>
                <TextField
                  required
                  fullWidth
                  label="Nombre Ocupación"
                  name="name"
                  type="text"
                  value={state.name}
                  onChange={handleInputChange}
                />
              </Grid>
              <Grid item xs={12} sm={12}>
                <div className={classes.creatingButtonContainer}>
                  {isLoading ? (
                    <CircularProgress size={26} />
                  ) : (
                    <Button
                      onClick={onSubmit}
                      variant="contained"
                      color="primary"
                      className={classes.createAccountButton}
                    >
                      Guardar
                    </Button>
                  )}
                </div>
              </Grid>
            </Grid>
          </form>
        </Paper>
        {/* Falso */}
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          open={openF}
          autoHideDuration={4000}
          onClose={handleCloseF}
        >
          <Notification
            onClose={handleCloseF}
            variant="error"
            message={`Completar campos Faltantes: 
             `}
          />
        </Snackbar>
        {/* Verdadero */}
        <Snackbar
          anchorOrigin={{
            vertical: "top",
            horizontal: "center"
          }}
          open={openV}
          autoHideDuration={4000}
          onClose={handleCloseV}
        >
          <Notification
            onClose={handleCloseF}
            variant="success"
            message="Creado"
          />
        </Snackbar>
      </Fragment>
    );
  } else {
    return <div>Loading...</div>;
  }
}
export default ContactForm;
