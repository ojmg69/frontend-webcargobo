//importacion
import React, { useState, Fragment } from "react";
import axios from "axios";
import {
  Grid,
  Button,
  TextField,
  Paper,
  CircularProgress,
  Snackbar
} from "@material-ui/core";
import Notification from "../../components/Notification/Notification";
import "date-fns";
import { makeStyles } from "@material-ui/styles";
import PageTitle from "../../components/PageTitle/PageTitle";
import { useHistory } from "react-router-dom";
import { UPLOAD_ENDPOINT } from "../../config";

//estilos
const useStyles = makeStyles(theme => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200
  },
  selectEmpty: {
    marginTop: theme.spacing(1)
  },
  paper: {
    padding: theme.spacing(2),
    display: "flex",
    overflow: "auto",
    flexDirection: "column",
    color: theme.palette.primary.main
  }
}));
export default function RegistroTransportista() {
  const initialFormState = {
    id: "",
    name: "",
    userId: ""
  };
  const [state, setState] = useState(initialFormState);
  const [isLoading, setIsLoading] = useState(false);
  const classes = useStyles();
  let history = useHistory();
  const [openV, setOpenV] = useState(false);
  const [openF, setOpenF] = useState(false);

  const [errores, setErrores] = useState([]);

  //enviar datos al servidor
  const uploadFile = async state => {
    const formData = new FormData();
    formData.append("name", state.name);
    formData.append("user_id", state.userId);

    const tipo = {
      headers: {
        Accept: "*/*",
        "Content-Type": "application/json"
      }
    };
    return axios
      .post(UPLOAD_ENDPOINT + "web/occupations/add", formData, tipo)
      .then(response => {
        console.log(response);
        return response;
      })
      .catch(err => {
        console.log("dentro del upload", err.response);
        return err.response;
      });
  };
  //Funciones
  const handleInputChange = event => {
    const { name, value } = event.target;
    setState(() => ({
      ...state,
      [name]: value,
      userId: localStorage.getItem("id")
    }));
  };

  const handleCloseV = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenV(false);
  };
  const handleCloseF = (event, reason) => {
    if (reason === "clickaway") {
      return;
    }

    setOpenF(false);
  };

  const onSubmit = async e => {
    e.preventDefault();
    setIsLoading(true);
    return await uploadFile(state).then(res => {
      if (res.status === 200) {
        console.log("correcto", res.status);
        setIsLoading(false);
        setOpenV(true);
        history.push("/app/ocupacion");
      } else {
        console.log("errores", res.data);
        setIsLoading(false);
        setErrores(res.data);
        setOpenF(true);
      }
    });
  };

  return (
    <Fragment>
      <Paper className={classes.paper}>
        <PageTitle
          title="Crear Ocupación"
          button="Regresar"
          url="/app/ocupacion"
        />
        <form onSubmit={onSubmit}>
          <Grid
            container
            direction="row"
            justify="flex-start"
            alignItems="flex-start"
            spacing={3}
          >
            {/* fila 1 inicio */}
            <Grid item xs={12} sm={3}>
              <TextField
                required
                fullWidth
                label="Nombre Ocupación"
                name="name"
                type="text"
                value={state.name}
                onChange={handleInputChange}
              />
            </Grid>
            <Grid item xs={12} sm={12}>
              <div className={classes.creatingButtonContainer}>
                {isLoading ? (
                  <CircularProgress size={26} />
                ) : (
                  <Button
                    onClick={onSubmit}
                    variant="contained"
                    color="primary"
                    className={classes.createAccountButton}
                  >
                    Guardar
                  </Button>
                )}
              </div>
            </Grid>
          </Grid>
        </form>
      </Paper>
      {/* Falso */}
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        open={openF}
        autoHideDuration={4000}
        onClose={handleCloseF}
      >
        <Notification
          onClose={handleCloseF}
          variant="error"
          message={
            `Completar campos Faltantes: 
             ` + errores
          }
        />
      </Snackbar>
      {/* Verdadero */}
      <Snackbar
        anchorOrigin={{
          vertical: "top",
          horizontal: "center"
        }}
        open={openV}
        autoHideDuration={4000}
        onClose={handleCloseV}
      >
        <Notification
          onClose={handleCloseF}
          variant="success"
          message="Creado"
        />
      </Snackbar>
    </Fragment>
  );
}
