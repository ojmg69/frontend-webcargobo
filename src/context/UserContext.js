import React from "react";
import axios from "axios";

const USER_API_BASE_URL = "https://webcargobo.com/api/";
var UserStateContext = React.createContext();
var UserDispatchContext = React.createContext();

function userReducer(state, action) {
  switch (action.type) {
    case "LOGIN_SUCCESS":
      return { ...state, isAuthenticated: true };
    case "SIGN_OUT_SUCCESS":
      return { ...state, isAuthenticated: false };
    case "LOGIN_FAILURE":
      return { ...state, isAuthenticated: false };
    default: {
      throw new Error(`Unhandled action type: ${action.type}`);
    }
  }
}

function UserProvider({ children }) {
  var [state, dispatch] = React.useReducer(userReducer, {
    isAuthenticated: !!localStorage.getItem("id")
    // eslint-disable-next-line no-undef
  });

  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

function useUserState() {
  var context = React.useContext(UserStateContext);
  if (context === undefined) {
    throw new Error("useUserState must be used within a UserProvider");
  }
  return context;
}

function useUserDispatch() {
  var context = React.useContext(UserDispatchContext);
  if (context === undefined) {
    throw new Error("useUserDispatch must be used within a UserProvider");
  }
  return context;
}

export { UserProvider, useUserState, useUserDispatch, loginUser, signOut };

// ###########################################################

async function loginUser(
  dispatch,
  login,
  password,
  history,
  setIsLoading,
  setError
) {
  setError(false);
  setIsLoading(true);

  if (!!login && !!password) {
    const user = {
      email: login,
      password: password
    };
    await userLLogin(user).then(res => {
      if (res) {
        setError(null);
        setIsLoading(false);
        dispatch({ type: "LOGIN_SUCCESS" });

        history.push("/app/dashboard");
      } else {
        dispatch({ type: "LOGIN_FAILURE" });
        setError(true);
        setIsLoading(false);
      }
    });
  }
}

function signOut(dispatch, history) {
  localStorage.removeItem("access_token");
  localStorage.removeItem("people_id");
  localStorage.removeItem("id");
  dispatch({ type: "SIGN_OUT_SUCCESS" });
  history.push("/login");
}
const userLLogin = user => {
  return axios
    .post(
      USER_API_BASE_URL + "auth/login",
      {
        email: user.email,
        password: user.password
      },
      {
        headers: { "Content-Type": "application/json" }
      }
    )
    .then(response => {
      localStorage.setItem("access_token", response.data.access_token);
      localStorage.setItem("id", response.data.user.id);
      localStorage.setItem("people_id", response.data.user.people_id);
      return response.data;
    })
    .catch(err => {
      console.log(err.response);
      // return err.response;
    });
};
